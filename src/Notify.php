<?php

namespace Drupal\notify;

use Drupal\comment\CommentInterface;
use Drupal\comment\Entity\Comment;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Mail\MailManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\State\StateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;
use Drupal\node\Entity\Node;
use Drupal\node\Entity\NodeType;
use Drupal\node\NodeInterface;
use Drupal\user\Entity\User;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Notify service class.
 */
class Notify implements NotifyInterface {
  use StringTranslationTrait;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The notify settings.
   *
   * @var \Drupal\Core\Config\Config
   */
  protected $config;

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * The datetime service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The mail manager service.
   *
   * @var \Drupal\Core\Mail\MailManager
   */
  protected $mailManager;

  /**
   * The node storage.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface|\Drupal\node\NodeStorage
   */
  protected $nodeStorage;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The account interface.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * The Drupal Logger Factory interface.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Class constructor.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\State\StateInterface $state
   *   The state service.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime service.
   * @param \Drupal\Core\Mail\MailManagerInterface $mail_manager
   *   The plugin mail manager service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The account service.
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $loggerFactory
   *   The Drupal Logger Factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   */
  public function __construct(Connection $database,
    ConfigFactoryInterface $config_factory,
    StateInterface $state,
    TimeInterface $time,
    MailManagerInterface $mail_manager,
    EntityTypeManagerInterface $entity_type_manager,
    AccountInterface $currentUser,
    LoggerChannelFactoryInterface $loggerFactory,
    ModuleHandlerInterface $module_handler,
    RequestStack $request_stack) {
    $this->database = $database;
    $this->config = $config_factory->get('notify.settings');
    $this->state = $state;
    $this->setDatetime($time);
    $this->mailManager = $mail_manager;
    $this->nodeStorage = $entity_type_manager->getStorage('node');
    $this->entityTypeManager = $entity_type_manager;
    $this->currentUser = $currentUser;
    $this->loggerFactory = $loggerFactory;
    $this->moduleHandler = $module_handler;
    $this->requestStack = $request_stack;
  }

  /**
   * Sets the datetime service.
   *
   * Mostly useful for use in automated tests.
   *
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The datetime service.
   */
  public function setDatetime(TimeInterface $time) {
    $this->time = $time;
  }

  /**
   * {@inheritdoc}
   */
  public function setUserNotify(int $uid, array $values): void {
    // Default values.
    $values += [
      'status' => 1,
      'node' => 0,
      'comment' => 0,
    ];

    // Check if a record already exists for user.
    $exists = $this->database->select('notify')
      ->condition('uid', $uid)
      ->countQuery()
      ->execute()
      ->fetchField();

    if ($exists) {
      // Update the record.
      $this->database->update('notify')
        ->fields([
          'status' => $values['status'],
          'node' => $values['node'],
          'comment' => $values['comment'],
        ])
        ->condition('uid', $uid)
        ->execute();
    }
    else {
      // Insert new record.
      $this->database->insert('notify')
        ->fields([
          'uid' => $uid,
          'status' => $values['status'],
          'node' => $values['node'],
          'comment' => $values['comment'],
        ])
        ->execute();
    }

    $def_nodetypes = $this->config->get('notify_nodetypes');

    // Loop over node types user subscribes to.
    $def_nodetypes = $def_nodetypes ?? [];
    foreach ($def_nodetypes as $nodetype => $value) {
      $key = static::NODE_TYPE . $nodetype;
      if (!isset($values[$key])) {
        // $nodetype no longer exists (extension  uninstalled).
        $subscribe = 0;
      }
      else {
        $subscribe = $values[$key];
      }
      $id = $this->database->select('notify_subscriptions', 'n')
        ->fields('n', ['id', 'uid', 'type'])
        ->condition('uid', $uid)
        ->condition('type', $nodetype)
        ->execute()->fetchObject();
      if ($id) {
        if (!$subscribe) {
          $sid = $id->id;
          $this->database->delete('notify_subscriptions')
            ->condition('id', $sid)
            ->execute();
        }
      }
      elseif ($subscribe) {
        $this->database->insert('notify_subscriptions')
          ->fields([
            'uid' => $uid,
            'type' => $nodetype,
          ])
          ->execute();
      }
    }

  }

  /**
   * {@inheritdoc}
   */
  public function bulkSubscribeUsers(): void {
    // Get uids of non-subscribers from {notify}.
    $uids = $this->database->select('notify', 'n')
      ->fields('n', ['uid'])
      ->condition('n.status', 0, '=')
      ->execute()
      ->fetchAllKeyed(0, 0);

    if (!empty($uids)) {
      // Subscribe them all using the default settings.
      $this->database->update('notify')
        ->fields([
          'status' => 1,
          'node' => $this->config->get('notify_def_node'),
          'comment' => $this->config->get('notify_def_comment'),
          'attempts' => 0,
        ])
        ->condition('uid', $uids, 'IN')
        ->execute();
    }
  }

  /**
   * {@inheritdoc}
   */
  public function skipNode(NodeInterface $node): NotifyInterface {
    $skipped = $this->getSkippedNodes();
    $skipped[$node->id()] = $node->id();
    $this->setSkippedNodes($skipped);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSkippedNodes(): array {
    return $this->state->get('notify_skip_nodes', []);
  }

  /**
   * {@inheritdoc}
   */
  public function setSkippedNodes(array $nids): NotifyInterface {
    $this->state->set('notify_skip_nodes', $nids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function skipComment(CommentInterface $comment): NotifyInterface {
    $skipped = $this->getSkippedComments();
    $skipped[$comment->id()] = $comment->id();
    $this->setSkippedComments($skipped);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSkippedComments(): array {
    return $this->state->get('notify_skip_comments', []);
  }

  /**
   * {@inheritdoc}
   */
  public function setSkippedComments(array $cids): NotifyInterface {
    $this->state->set('notify_skip_comments', $cids);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getContentTypes(bool $full_list_when_empty): array {
    $node_types = [];
    foreach (NodeType::loadMultiple() as $type => $object) {
      if ($this->config->get(static::NODE_TYPE . $type)) {
        $node_types[] = $type;
      }
    }
    if ($full_list_when_empty && empty($node_types)) {
      foreach (NodeType::loadMultiple() as $type => $name) {
        $node_types[] = $type;
      }
    }

    return $node_types;
  }

  /**
   * {@inheritdoc}
   */
  public function selectContent(): array {
    $send_start = $this->time->getRequestTime();
    // Deal with race condition, new content created since we started to send.
    $since = $this->state->get('notify_send_last', 0);
    // If never sent before, set $since to not notify about really old stuff.
    if (!$since) {
      $period = $this->config->get('notify_period');
      if ($period > 0) {
        $since = $send_start - $period;
      }
    }

    $all = NodeType::loadMultiple();
    $ntype = [];
    foreach ($all as $type => $object) {
      $ntype[] = $type;
    }

    // Fetch new published nodes.
    $nodes = $this->entityTypeManager->getStorage('node')->loadMultiple();
    $res_nodes = $obj_nopub = $obj_nounp = [];
    foreach ($nodes as $node) {
      $crearray = $node->get('created')->getValue();
      $created = $crearray[0]['value'];
      $updarray = $node->get('changed')->getValue();
      $updated = $updarray[0]['value'];
      if ($this->config->get('notify_include_updates')) {
        if ($created >= $since || $updated >= $since) {
          $res_nodes[] = $node->id();
        }
      }
      else {
        if ($created >= $since) {
          $res_nodes[] = $node->id();
        }
      }
    }

    // Get published nodes in unpublished queue.
    $q = $this->database->select('notify_unpublished_queue', 'q');
    $q->join('node', 'n', 'q.nid = n.nid');
    $q->join('node_field_data', 'u', 'q.nid = u.nid');
    $q->fields('q', ['nid', 'cid']);
    $q->condition('u.status', 1, '=');
    $q->condition('q.cid', 0, '=');
    $q->orderBy('q.nid', 'asc');
    $obj_nopub = $q->execute()->fetchAllAssoc('nid');

    // Get unpublished nodes in unpublished queue.
    $q = $this->database->select('notify_unpublished_queue', 'q');
    $q->join('node', 'n', 'q.nid = n.nid');
    $q->join('node_field_data', 'u', 'q.nid = u.nid');
    $q->fields('q', ['nid', 'cid']);
    $q->condition('u.status', 0, '=');
    $q->condition('q.cid', 0, '=');
    $q->orderBy('q.nid', 'asc');
    $obj_nounp = $q->execute()->fetchAllAssoc('nid');

    $res_comms = $obj_copub = $obj_counp = [];
    if ($this->moduleHandler->moduleExists('comment')) {
      // Fetch new published comments.
      $comments = $this->entityTypeManager->getStorage('comment')->loadMultiple();
      foreach ($comments as $comment) {
        $crearray = $comment->get('created')->getValue();
        $created = $crearray[0]['value'];
        $updarray = $comment->get('changed')->getValue();
        $updated = $updarray[0]['value'];
        if ($this->config->get('notify_include_updates')) {
          if ($created >= $since || $updated >= $since) {
            $res_comms[] = $comment->id();
          }
        }
        elseif ($created >= $since) {
          $res_comms[] = $comment->id();
        } // if include updates
      } // foreach

      // Get published comments in unpublished queue.
      $q = $this->database->select('notify_unpublished_queue', 'q');
      $q->join('comment', 'c', 'q.cid = c.cid');
      $q->join('comment_field_data', 'v', 'q.cid = v.cid');
      $q->fields('q', ['cid', 'nid']);
      $q->condition('v.status', 1, '=');
      $q->orderBy('q.cid', 'asc');
      $obj_copub = $q->execute()->fetchAllAssoc('cid');

      // Get unpublished comments in unpublished queue.
      $q = $this->database->select('notify_unpublished_queue', 'q');
      $q->join('comment', 'c', 'q.cid = c.cid');
      $q->join('comment_field_data', 'v', 'q.cid = v.cid');
      $q->fields('q', ['cid', 'nid']);
      $q->condition('v.status', 0, '=');
      $q->orderBy('q.cid', 'asc');
      $obj_counp = $q->execute()->fetchAllAssoc('cid');
    } // if comment module exists
    return [
      $res_nodes,
      $res_comms,
      $obj_nopub,
      $obj_copub,
      $obj_nounp,
      $obj_counp,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function countContent(): array {
    [$res_nodes, $res_comms, $obj_nopub, $obj_copub, $obj_nounp, $obj_counp] = $this->selectContent();

    $np = ($res_nodes) ? count($res_nodes) : 0;
    $cp = ($res_comms) ? count($res_comms) : 0;
    $nn = ($obj_nopub) ? count($obj_nopub) : 0;
    $cn = ($obj_copub) ? count($obj_copub) : 0;
    $bu = ($obj_nounp) ? count($obj_nounp) : 0;
    $cu = ($obj_counp) ? count($obj_counp) : 0;

    return [$np, $cp, $nn, $cn, $bu, $cu];
  }

  /**
   * {@inheritdoc}
   */
  public function nextNotification(int $send_last): int {
    $period = $this->config->get('notify_period');
    // Two special cases: Never and instantly.
    if ($period < 0) {
      return static::PERIOD_NEVER;
    }
    elseif (!$period) {
      return static::PERIOD_ALWAYS;
    }
    $next_time_to_send = $send_last + $period;
    if ($period < 86400) {
      if ($this->time->getRequestTime() >= $next_time_to_send) {
        return static::PERIOD_ALWAYS;
      }
      else {
        return $next_time_to_send;
      }
    }

    // Interval >= 1 day.
    $cron_next = $this->state->get('notify_cron_next', 0);
    if (!$cron_next) {
      $cron_next = $this->cronNext($next_time_to_send);
      $this->state->set('notify_cron_next', $cron_next);
    }

    return($cron_next);
  }

  /**
   * {@inheritdoc}
   */
  public function cronNext(int $next_time_to_send): int {
    $send_hour = $this->config->get('notify_send_hour');
    // Compute the next as the sending hour today.
    $cron_next = strtotime(date('Y-m-d ', $next_time_to_send) . $send_hour . ':00:00');

    return $cron_next;
  }

  /**
   * {@inheritdoc}
   */
  public function send(): array {
    $user = $this->currentUser;
    $host = $this->requestStack->getCurrentRequest()->getSchemeAndHttpHost();

    $send_start = $this->state->get('notify_send_start', 0);
    $num_sent = 0;
    $num_fail = 0;
    [$res_nodes, $res_comms, $obj_nopub, $obj_copub, $obj_nounp, $obj_counp] = $this->selectContent();

    $defaultlist = $this->getContentTypes(TRUE);

    // Get the nodes and comments queued.
    $count = 0;
    $nodes = $comments = [];
    /*
     * Ordinary nodes.
     * dpm($res_nodes, 'res_nodes');
     * dpm($res_comms, 'res_comms');
     */
    foreach ($res_nodes as $nid) {
      $nodes[$nid] = Node::load($nid);
      $count++;
    }

    // Published nodes in unpublished queue.
    foreach ($obj_nopub as $row) {
      if (!isset($nodes[$row->nid])) {
        $nodes[$row->nid] = Node::load($row->nid);
        $count++;
      }
    }

    // Unpublished nodes in unpublished queue.
    foreach ($obj_nounp as $row) {
      if (!isset($nodes[$row->nid])) {
        $nodes[$row->nid] = Node::load($row->nid);
        $count++;
      }
    }

    // Ordinary comments.
    foreach ($res_comms as $row) {
      $comment = Comment::load($row);
      /* dpm($comment, 'comment'); */
      $pnid = $comment->get('entity_id')->target_id;
      $comments[$row]['parent'] = $pnid;
      $comments[$row]['comment'] = $comment;
      /* dpm($pnid, 'nid of parent node'); */
      $count++;
    }

    /*
     * Need more work
     * // Published comments in unpublished queue.
     * foreach ($obj_copub as $row) {
     *   if (!isset($comments[$row][$row->cid])) {
     *     $comments[$row->get('entity_id')->target_id][$row->cid] =
     *       Comment::load($row->cid);
     *     $count++;
     *   }
     * }
     *
     * // Unpublished comments in unpublished queue.
     */

    /* dpm($nodes, 'nodes'); */
    /* dpm($comments, 'comments'); */

    if ($count) {
      $uresult = $this->state->get('notify_users');
      if (empty($uresult)) {
        // Set up for sending a new batch. Init all variables.
        $result = $this->database->select('notify', 'n');
        $result->join('users', 'u', 'n.uid = u.uid');
        $result->join('users_field_data', 'v', 'n.uid = v.uid');
        $result->fields('u', ['uid']);
        $result->fields('v', ['name', 'mail']);
        $result->fields('n', ['node', 'comment', 'status']);
        $result->condition('v.status', 1);
        $result->condition('n.status', 1);
        $result->condition('n.attempts', 5, '<=');
        $uresult = $result->execute()->fetchAll(\PDO::FETCH_ASSOC);

        $this->state->setMultiple([
          'notify_send_start' => $this->time->getRequestTime(),
          'notify_users' => $uresult,
          'notify_num_sent' => 0,
          'notify_num_failed' => 0,
        ]);
      }

      // Allow to safely impersonate the recipient so that the node is rendered
      // with correct field permissions.
      $original_user = $user;
      $notify_skip_nodes = $this->getSkippedNodes();
      $notify_skip_comments = $this->getSkippedComments();

      foreach ($uresult as $index => $userrow) {
        $userobj = User::load($userrow['uid']);

        // Intentionally replacing the Global $user.
        $user = $userobj;
        $upl = $userobj->getPreferredLangcode();

        $node_body = $comment_body = '';

        // Consider new node content if user has permissions and node is ready.
        // $userrow['node']: user subscribes to nodes.
        if ($userrow['node'] && $userobj->hasPermission('access content') && count($nodes)) {

          $node_count = 0;
          // Look at the node.
          foreach ($nodes as $node) {
            // Skip to next if skip flag set for node.
            if (in_array($node->id(), $notify_skip_nodes)) {
              continue;
            }
            // Skip to next if user is not allowed to view this node.
            if (!$userobj->hasPermission('administer nodes') && 0 == $node->isPublished()) {
              continue;
            }
            // Skip if user is not admin and node is not on defaultlist.
            if (!$userobj->hasPermission('administer nodes') && !in_array($node->getType(), $defaultlist)) {
              continue;
            }
            // Skip if ???
            if (!$this->config->get('notify_unpublished') && 0 == $node->isPublished()) {
              continue;
            }
            // Skip if ???
            if (!$node->access('view', $userobj)) {
              continue;
            }

            $field = $this->database->select('notify_subscriptions', 'n')
              ->fields('n', ['uid', 'type'])
              ->condition('uid', $userrow['uid'])
              ->condition('type', $node->getType())
              ->execute()->fetchAssoc();

            if ($field) {
              $ml_level = $this->config->get('notify_multilingual');
              if (!$userobj->hasPermission('administer notify')) {
                if ($ml_level && $node->tnid) {
                  if ($node->language != $upl) {
                    continue;
                  }
                }
                if ((2 == $ml_level) && (0 == $node->tnid) && ('und' != $node->language)) {
                  continue;
                }
                $ignore_unverified = $this->config->get('notify_unverified');
                if ($ignore_unverified && !$node->uid) {
                  continue;
                }
              }

              // Start accumulating the body for the notification.
              $node_body .= ++$node_count . '. ' . $node->label();

              // Creates a link to be embedded in plain text mail:
              $link = $host . '/' . Url::fromUri('internal:/node/' . $node->id())->toString();
              $node_body .= ' - see ' . $link . '<br>';
            } // if ($field)
          } // foreach ($nodes as $node)
          // Prepend email header as long as user can access at least one node.
          if ($node_count > 0) {
            $node_body = '<p>' . $this->t('<p>Recent new or updated pages - @count', [
              '@count' => $this->formatPlural($node_count, '1 new post', '@count new posts', [], ['langcode' => $upl]),
            ], ['langcode' => $upl]) . "<br />" . '</p>' . $node_body;
          }
        }
        // Need to filter on content type and pernission,
        // see issues #3225558 and #3221814.
        // Consider new comments if user has permissions and comments are ready.
        if ($userrow['comment'] && $userobj->hasPermission('access comments') && count($comments)) {
          $comment_count = $node_comment_count = $commentindex = 0;
          $commentlinks = [];
          foreach ($comments as $value) {
            $pnid = $value['parent'];
            // If we don't already have the node, fetch it.
            if (isset($nodes[$pnid])) {
              $node = $nodes[$pnid];
              /* dpm($pnid, 'have node'); */
            }
            else {
              $node = $this->entityTypeManager
                ->getStorage('node')
                ->load($pnid);
              /* dpm($pnid, 'load node'); */
            }
            if (!$node->access('view', $userobj)) {
              continue;
            }

            $notify = $this->database->select('notify_subscriptions', 'n')
              ->fields('n', ['uid', 'type'])
              ->condition('uid', $userrow['uid'])
              ->condition('type', $node->getType())
              ->execute()->fetchAssoc();

            if ($notify) {
              $commobj = $value['comment'];

              // Look at the comment.
              $permalink = $commobj->permalink()->toString();
              if (in_array($commobj->id(), $notify_skip_comments)) {
                continue;
              }
              if (!$userobj->hasPermission('administer comments') && 0 == $commobj->isPublished()) {
                continue;
              }
              if (!$userobj->hasPermission('administer comments') && !in_array($node->getType(), $defaultlist)) {
                continue;
              }
              if (!$this->config->get('notify_unpublished') && 0 == $commobj->isPublished()) {
                continue;
              }

              // Creates a link to be embedded in plain text mail:
              $commentlink = $host . $permalink;
              $commentlinks[$commentindex]['title'] = $node->label();
              $commentlinks[$commentindex]['link'] = $commentlink;
              $commentindex++;
              $comment_count++;
            }
          }

          if ($comment_count) {
            $comment_body .= '<br />&nbsp;<br /><p>' . $this->t('Here are links to the @count posted:', [
              '@count' => $this->formatPlural($comment_count, '1 new comment', '@count new comments'),
            ], ['langcode' => $upl]) . "<br />" . '</p>' . $comment_body;
            foreach ($commentlinks as $commentlink) {
              $comment_body .= ++$node_comment_count . '. ';
              $comment_body .= $this->t('@title - see @link', [
                '@title' => $commentlink['title'],
                '@link' => $commentlink['link'],
              ]) . '<br />';
            }
          }
        }

        $body = $node_body . $comment_body;
        // If there was anything new, send mail.
        if ($body) {
          $watchdog_level = $this->config->get('notify_watchdog');
          if ($this->mailManager->mail('notify', 'notice', $userrow['mail'], $upl,
            ['content' => $body, 'user' => $userobj], NULL, TRUE)) {
            if ($watchdog_level == 0) {
              $this->loggerFactory->get('notify')->notice('User %name (%mail) notified successfully.',
                ['%name' => $userrow['name'], '%mail' => $userrow['mail']]);
            }
            $num_sent++;
          }
          else {
            $num_fail++;
            $q = $this->database->update('notify');
            $q->expression('attempts', 'attempts + 1');
            $q->condition('uid', $userrow['uid']);
            $q->execute();

            if ($watchdog_level <= 2) {
              $this->loggerFactory->get('notify')->notice('User %name (%mail) could not be notified. Mail error.',
                ['%name' => $userrow['name'], '%mail' => $userrow['mail']]);
            }
          }
        }

        unset($uresult[$index]);
        $this->state->set('notify_users', $uresult);
      }
      // Restore the original user session.
      $user = $original_user;
    }
    $users = $this->state->get('notify_users');
    $rest = $users ? count($users) : 0;
    // If $rest is empty, then set notify_send_last.
    if (!$rest) {
      $send_start = $this->state->get('notify_send_start', 0);
      $this->state->set('notify_send_last', $send_start);
      // Force reset.
      $this->state->set('notify_cron_next', 0);

      [$res_nodes, $res_comms, $obj_nopub, $obj_copub, $obj_nounp, $obj_counp] = $this->selectContent();
      foreach ($obj_nopub as $row) {
        $q = $this->database->delete('notify_unpublished_queue');
        $q->condition('cid', 0);
        $q->condition('nid', $row->nid);
        $q->execute();
      }
      if ($obj_copub) {
        foreach ($obj_copub as $row) {
          $q = $this->database->delete('notify_unpublished_queue');
          $q->condition('cid', $row->cid);
          $q->condition('nid', $row->nid);
          $q->execute();
        }
      }
    }

    return [$num_sent, $num_fail];
  }

}
