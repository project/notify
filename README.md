# Notify

## Contents of this file

- Introduction
- Requirements
- Recommended modules
- Installation
- Configuration
- Maintainers


## Introduction

**Notify** is a lightweight module for sending plain text email
notifications about new nodes and comments posted on a Drupal web
site.

Users may subscribe to notifications about all new content, or only
subscribe to specific content types.

- For a full description of the module, visit the [project
  page](https://drupal.org/project/notify).

- To submit bug reports and feature suggestions, or to track changes
  visit the project's [issue
  tracker](https://drupal.org/project/issues/notify).

- For community documentation, visit the [documentation
  page](https://www.drupal.org/docs/contributed-modules/notify).

If you enable [node revisions](https://www.drupal.org/node/320614),
the notification email will also mention the name of the last person
to edit the node.


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

- [**Queue Mail**](https://drupal.org/project/queue_mail):  
  For better performance if sending notifications to many recipients.


## Installation

1. Install as you would normally install a contributed Drupal
   module. Visit [Installing
   modules](https://www.drupal.org/node/1897420) for further
   information.

2. Enable Notify module by locating it on the list under the Extend
   tab in the administrative GUI. The tables will be created
   automagically for you at this point.

3. Set permissions on the **People » Permissions page**.

   To set the notification checkbox default on new user registration
   form, or let new users opt in for notifications during
   registration, you must grant the anonymous user the right to
   "Access Notify".

   To allow authenticated users manage what content types they
   subscribe to, you must give the authenticated user the right to
   "Access Notify".

4. Configure general notification settings.  See the next section for
   details.


## Configuration

The administrative interface is at: **Manage » Configuration »
People » Notify Settings**.

- The "Settings" tab is for setting how often notifications are sent.

- The and "Default settings" tab is to set up default presets for new
  users, and for selecting permitted notification by node type.

- The "Users" tab is to review and see per-user settings.

- The remaining two tabs ("Queue" and "Skip flags") are for managing
  the notification queue.

To use **Queue Mail** to send the notifications, install it and
navigate to: **Manage » Configuration » System » QueueMail**.  Add
"`notify_*`" to the list of Mail IDs to queue.

When setting how often periodic notifications are sent, note that this
can only happen as frequently as cron is set to run.  Check, and if
necessary, configure your cron settings.


### Per user settings

To manage *your own* notification preferences, click on the
"Notification settings" on your "My account" page.


## Maintainers

Maintainers:

- Matt Chapman ([matt2000](https://www.drupal.org/u/matt2000)) is the
  project owner.

- Gisle Hannemyr ([gisle](https://www.drupal.org/u/gisle)) maintains all
  supported branches.

Past contributors:

- Kjartan Mannes ([Kjartan](https://www.drupal.org/u/kjartan)) created the projct.
- Rob Roy Barreca ([RobRoy](https://www.drupal.org/u/robroy)) was a previous maintainer.
- Larisse Amorim ([Larisse](https://www.drupal.org/u/larisse)) was a previous maintainer.

Supporting organizations:

- This project has been sponsored by [Hannemyr Nye Medier
  AS](https://hannemyr.no).
